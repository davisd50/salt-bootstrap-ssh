##HISTORY

1.0.0
----------

* initial release

1.1.0
----------

* change method to read master public key from sftp to ssh with cat command
* add --master-ssh-port option

1.1.1
----------

* fix issue with HOME/.ssh creation

1.1.2
----------

* fix issue installing openssh client/server on Ubuntu 20